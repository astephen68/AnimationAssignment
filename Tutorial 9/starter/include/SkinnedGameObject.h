#pragma once

#include "GameObject.h"
#include "HTRLoader.h"
#include "TTK\OBJMesh.h"
#include "KeyframeController.h"

// Forward declare the HTRLoader class
// This is necessary because SkinnedGameObject references HTRLoader and HTRLoader references SkinnedGameObject
class HTRLoader;

class SkinnedGameObject : public GameObject
{
	// Let the HTRLoader class access private and protected member variables
	friend class HTRLoader;

protected:
	int m_pCurrentFrame;
	HTRLoader* m_pHTRAnimation;
	JointDescriptor* m_pJointAnimation; // The animation for this specific joint

	std::shared_ptr<TTK::OBJMesh> m_pBindMesh; // Mesh in t-pose
	std::shared_ptr<TTK::OBJMesh> m_pSkinnedMesh;

	glm::mat4 m_pJointToBindMat;

	KeyFrameController<glm::vec3> myController;
	KeyFrameController<glm::quat> myController2;
	KeyFrameController<float> myController3;

public:
	SkinnedGameObject();
	~SkinnedGameObject();

	bool playing = true;
	bool pause=false;
	bool reverse = false;
	bool restart=false;

	void initializeSkeletonFromHTR(std::string htrFilePath, std::string skinWeightFilePath, std::shared_ptr<TTK::OBJMesh> bindMesh);

	glm::mat4 getJointToBindMatrix();

	virtual void update(float dt);
};