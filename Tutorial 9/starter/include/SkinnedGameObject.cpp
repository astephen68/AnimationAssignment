/* Name: Stephen Richards ID: 100458273
Daniel Munusami      100552012
Gerin Paul Puig      100656910
Selina Daley         100558926
*/



#include "SkinnedGameObject.h"
#include <GLM/glm.hpp>
#include <GLM\gtx\transform.hpp>
#include "HTRLoader.h"
#include <iostream>
#include <GLEW\glew.h>
#include <tinyxml2\tinyxml2.h>
#include "TTK/GraphicsUtils.h"
#include "GLM/gtx/string_cast.hpp"

SkinnedGameObject::SkinnedGameObject()
	: m_pCurrentFrame(0),
	m_pHTRAnimation(nullptr),
	m_pJointAnimation(nullptr),
	playing(true),
	m_pJointToBindMat(glm::mat4(1.0f))
{
}

SkinnedGameObject::~SkinnedGameObject()
{
	delete m_pHTRAnimation;
	delete m_pJointAnimation;
}

void SkinnedGameObject::initializeSkeletonFromHTR(std::string htrFilePath, std::string skinWeightFilePath, std::shared_ptr<TTK::OBJMesh> bindMesh)
{
	// Load the HTR data
	m_pHTRAnimation = new HTRLoader();
	m_pHTRAnimation->loadHTR(htrFilePath); // TODO: IMPLEMENT THIS FUNCTION

	// Create GameObjects from the data
	m_pHTRAnimation->createGameObjects();  // TODO: IMPLEMENT THIS FUNCTION
	std::vector<SkinnedGameObject*> temp = m_pHTRAnimation->getGameObjects();
	for (int j = 0; j < temp.size(); j++)
	{
		if (temp[j]->m_pJointAnimation!=nullptr)
		{
			for (int i = 0; i < temp[j]->m_pJointAnimation->numFrames; i++)
			{

				temp[j]->myController.AddKey(temp[j]->m_pJointAnimation->jointPositions.at(i), i);
				temp[j]->myController2.AddKey(temp[j]->m_pJointAnimation->jointRotations.at(i), i);
				temp[j]->myController3.AddKey(temp[j]->m_pJointAnimation->jointScales.at(i), i);
				temp[j]->myController.CalculateLookupTable(10);
				temp[j]->myController2.CalculateLookupTable(10);
				temp[j]->myController3.CalculateLookupTable(10);

			}
		}
		
	}
	


	// Add the root node from the HTR file as a child to
	// this GameObject. This allows us to control the locomotion of the hierarchy
	// my changing 'this' GameObject's scale, rotation and translation
	addChild(m_pHTRAnimation->getRootGameObject());
}

glm::mat4 SkinnedGameObject::getJointToBindMatrix()
{
	return m_pJointToBindMat;
}

void SkinnedGameObject::update(float dt)
{
	// This GameObject has no jointAnimation so we will construct our
	// transformation matrices in the traditional way


	if (restart==true)
	{
		std::vector<SkinnedGameObject*> temp=m_pHTRAnimation->getGameObjects();
		for (int i = 0; i < temp.size(); i++)
		{
			temp[i]->m_pCurrentFrame=0;
		}
	}


	if (pause == true &&playing==false)
	{
		playing = false;

	}
	if (reverse==true)
	{
		std::vector<SkinnedGameObject*> temp2 = m_pHTRAnimation->getGameObjects();
		for (int i = 0; i < temp2.size(); i++)
		{
			if (temp2[i]->m_pJointAnimation!=nullptr)
			{
				m_pCurrentFrame--;
				if (m_pCurrentFrame <= 0)
				{
					m_pCurrentFrame = temp2[i]->m_pJointAnimation->numFrames - 1;

				}
			}
			temp2[i]->m_pCurrentFrame = m_pCurrentFrame;
		}

	}
	else
	{
		if (m_pJointAnimation!=nullptr)
		{
			m_pCurrentFrame++;
			if ((m_pCurrentFrame) >= (int)m_pJointAnimation->numFrames)
				(m_pCurrentFrame) = 0;

		}
	}
	



	if (playing==true&&pause==false)
	{
		pause = false;
		if (m_pJointAnimation == nullptr)
		{
			if (!playing)
				dt = 0.0f;

			GameObject::update(dt*0.1f);
		}
		else
		{
			//// Todo: create localRotation, scale and translation matrices using HTR data


			glm::vec3 pos=myController.Update(dt);
			myController2.Update(dt);
			float sca= myController3.Update(dt);


			m_pLocalRotation = glm::mat4_cast(m_pJointAnimation->jointBaseRotation
				*m_pJointAnimation->jointRotations[m_pCurrentFrame]);

			//m_pLocalPosition = 0.1f* m_pJointAnimation->jointPositions[m_pCurrentFrame];
			m_pLocalPosition = pos;


			m_pLocalScale = glm::vec3(m_pJointAnimation->jointScales[m_pCurrentFrame],
				m_pJointAnimation->jointScales[m_pCurrentFrame],
				m_pJointAnimation->jointScales[m_pCurrentFrame]);

			//// Create translation matrix
			glm::mat4 tran = glm::translate(m_pLocalPosition);
			//// Create scale matrix
			glm::mat4 scal = glm::scale(m_pLocalScale);


			m_pLocalTransformMatrix = tran*m_pLocalRotation*scal;
			m_pJointToBindMat = glm::inverse(m_pLocalTransformMatrix);

			if (m_pParent)
			{

				m_pLocalToWorldMatrix = m_pParent->getLocalToWorldMatrix()*m_pLocalTransformMatrix;

			}
			else
			{
				m_pLocalToWorldMatrix = m_pLocalTransformMatrix;

			}

			// Update children
			for (int i = 0; i < m_pChildren.size(); i++)
			{
				m_pChildren[i]->update(dt*0.1);
			}
	
				//m_pCurrentFrame++;



			
			// Increment frame to next frame
			// Note: Right now we are just playing the animation back frame by frame
			// Since we exported the animation at 60 fps, as long as the game runs at 60 fps this works fine
			// However, to make this more robust, you should blend between frames (i.e. lerp / slerp) to get
			// frame rate independent playback. This will also allow you to control the
			// playback speed of the animation (hint: this is what you need to do for assignment 3!)




	


		}
		
	}

}